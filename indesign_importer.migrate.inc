<?php
/*
 * Implements hook_migrate_api()
 */
function indesign_importer_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'indesign' => array(
        'title' => t('Indesign Migration'),
      ),
    ),
  );
  return $api;
}