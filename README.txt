Overview:
The InDesign Importer module creates a base class which has no use unless extended by a child class.
The child class needs to define the destination ie: node type, as well as any additional fields.

Requirements:
The site must have an 'issue' and 'article' node type (name is irrelevant), and the article node type
must have a entity reference field back to the issue.

Steps:
1) Install indesign_importer and indesign_importer_ui modules
2) Create a module which with a .inc file; include this file in the .info file
3) Create a custom class which extends the IndesignXMLMigration class, and define $this->destination
   Example:
   $this->destination = new MigrateDestinationNode('magazine_article');
4) Implement the InDesign Importer UI hook to set the base class. The class value should be the same
   name as the class in your .inc file
5) Run the importer - this will dynamically register your migration.
6) The migration is now available on the Migrate UI page. The Migrate module provides all the tools to import and
   rollback your migration.

Example XML Structure:
<Root>
    <ArticleContainer>
        <Story>
            <Byline>
                SubTitle Text
            </Byline>
        </Story>
        <Story>
            <Paragraph>
                Paragraph Text with <Bold>bold</Bold> text included inline.
            </Paragraph>
            <Paragraph>
                Paragraph Text with <Italic>bold</Italic> text included inline.
                <ImageContainer>
                    <Image href_opt="images/filename.jpg" href="absolute/path/to/filename.tif"></Image>
                    <ImageCaption>
                        Caption text
                    </ImageCaption>
                </ImageContainer>
            </Paragraph>
        </Story>
    </ArticleContainer>
    <ArticleContainer>
        ...
    </ArticleContainer>
</Root>