<?php
/**
 * @file
 * Provides a abstract migration class to be used for InDesign migration
 */

/**
 * Class IndesignXMLMigration
 * Base InDesign migration class
 */
abstract class IndesignXMLMigration extends XMLMigration {
  protected $item_url;
  protected $item_xpath;
  protected $item_ID_xpath;
  protected $fields;
  protected $issue_nid;
  protected $images;

  /**
   * A Migration constructor takes an array of arguments as its first parameter.
   * The arguments must be passed through to the parent constructor.
   * @param array $arguments - the migration arguments
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('XML Import from InDesign');
    $this->images = $arguments['images'];
    $this->items_url = $arguments['source_file'];
    $this->item_xpath = '/Root/ArticleContainer';
    $this->item_ID_xpath = 'ArticleId';
    $this->fields = $this->_getIndesignXMLFields();

    $this->source = new MigrateSourceXML($this->items_url, $this->item_xpath, $this->item_ID_xpath, $this->fields);

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'ArticleId' => array(
          'type' => 'varchar',
          'length' => 4,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->issue_nid = $arguments['issue_nid'];
    //we will not be migrating the body field, this will need to be prepared separately
    $this->addUnmigratedDestinations(array( //TODO - MAKE THIS AN CALLBACK FUNCTION
      'body:summary',
      'body:format',
      'changed',
      'comment',
      'created',
      'is_new',
      'language',
      'log',
      'promote',
      'revision',
      'revision_uid',
      'sticky',
      'tnid',
      'translate',
      'uid',
    ));

    if (module_exists('domain')) {
      $this->addUnmigratedDestinations(array(
        'domain_site',
        'domains',
        'domain_source',
      ));
    }

    if (module_exists('pathauto')) {
      $this->addUnmigratedDestinations(array(
        'path',
        'pathauto',
      ));
    }
  }

  /**
   * Returns the source field list to be used for the migration
   * @return array - the list
   */
  protected function _getIndesignXMLFields() {
    $fields = array(
      'title' => t('Title'),
      'body' => t('Body'),
    );
    return $fields;
  }

  /**
   * Returns the file fid or array of file fids associated with the image filename(s)
   * @param string|array $field_item - filename(s)
   * @return array|int|null - fid/fids
   */
  public function lookupImageFids($field_item) {
    if (is_array($field_item)) {
      $fids = array();
      foreach ($field_item as $item) {
        $fids[] = $this->lookupImageFid($item);
      }
      return $fids;
    }
    else {
      $fid = $this->lookupImageFid($field_item);
      return $fid;
    }
  }

  /**
   * Looks up the file fid associated with the image filename
   * @param string $field_item - the href of the image in the XML
   * @return null|integer - the fid
   */
  public function lookupImageFid($field_item) {
    //get the fid from looking up via xml
    $field_item = strtolower($field_item);
    $field_item = str_replace('__', '_', $field_item); //uploaded files get renamed, so we need to account for this when doing a lookup
    $uploaded_images = $this->images;
    foreach ($uploaded_images as $image) {
      $filename = strtolower($image->filename);
      if (strpos($field_item, $filename) !== FALSE) {
        return $image->fid;
      }
    }
    return NULL;
  }

  /**
   * Search and replace an XML string and replace tags with provided HTML tag mapping
   * @param array $search - the XML tags to search for
   * @param array $replace - the HTML tags to replace with
   * @param string $xml_string - the XML string
   * @return string - the modified string
   */
  public function xmlToHtml($replacements, $xml_string) {
    foreach ($replacements as $key => $value) {
      $xml_string = preg_replace('#(<|(<\/))(' . $key . ')(>)#', ' $1' . $value . '$4 ', $xml_string); //Note: spaces were deliberately added before and after tags
    }
    return $xml_string;
  }

  /**
   * Removes an entire XML tag from an XML String, including its children
   * @param string $tag_name - name of the tag
   * @param string $xml_string - the XML string
   * @return string - the modified string
   */
  public function removeXmlTag($tag_name, $xml_string) {
    $xml_string = preg_replace('#(<' . $tag_name . ')([\s\S]+?)(<\/' . $tag_name . '>)#', '', $xml_string); //remove XML tags an contents
    return $xml_string;
  }

  /**
   * Returns the filter format ID
   * @param string $format_name - human readable format name
   * @return string - the format ID
   */
  public function getFilterFormat($format_name) {
    $format_value = '';
    $filter_formats = filter_formats();
    foreach ($filter_formats as $format) {
      if ($format->name === $format_name) {
        $format_value = $format->format;
        break;
      }
    }
    return $format_value;
  }

  /**
   * Removes linebreaks from text
   * @param string $string - text
   * @return string - the modified string
   */
  public function removeLinebreaks($string) {
    $pattern = '[\t\n\r\0\x0B]';
    $string = preg_replace($pattern, '', $string);
    return $string;
  }

  /**
   * Translates SimpleXMLElements to HTML for a WYSIWYG field
   * Assumes the <Paragraph> tag structure as defined om the README file.
   * @param array $xml_elements - an array of SimpleXMLElements used to construct the WYSIWYG body
   * @return string - the formatted result
   * @throws \Exception
   */
  public function prepareWysiwygBody($xml_elements) {
    $text_value = '';
    foreach ($xml_elements as $element) {
      //first add in the paragraph text
      $paragraph_xml = $element->asXML();
      if ($paragraph_xml === "<Paragraph/>") {
        continue; //don't save empty paragraphs
      }
      $text_value .= $paragraph_xml;
      //now add in the images
      $images = $element->xpath('ImageContainer');
      if (!empty($images)) {
        foreach ($images as $image) {
          $image_caption = (string) $image->Story->ImageCaption;
          $image_href = (string) $image->Image['href_opt'];
          $image_class = (string) $image->Image['class'];
          $image_fid = $this->lookupImageFid($image_href);
          if ($image_fid) {
            $image_file = file_load($image_fid);
          }
          $inline_image = '';
          if (!empty($image_file)) {
            $variables = array(
              'path' => file_create_url($image_file->uri),
              'attributes' => array()
            );
            $inline_image = '<figure' . (!empty($image_class) ? ' class=' . $image_class : "") . '>';
            $inline_image .= theme_image($variables);
            if (!empty($image_caption)) { //create a figure element to hold the image and caption
              $variables['alt'] = $image_caption;
              $variables['title'] = $image_caption;
              $inline_image .= '<figcaption>';
              $inline_image .= $image_caption;
              $inline_image .= '</figcaption>';
            }
            $inline_image .= '</figure>';
          }
          else {
            watchdog('migrate', 'Missing image: href = @image_href', array('@image_href' => $image_href), WATCHDOG_WARNING);
          }
          $text_value .= $inline_image;
        }
      }
    }
    return $text_value;
  }

  /**
   * Returns a map from InDesign XML tags to corresponding HTML tags
   * @return array
   */
  public function getXmlToHtmlMapping() {
    $replacements = array(
      'Paragraph' => 'p',
      'Bold' => 'strong',
      'Italic' => 'em',
    );
    return $replacements;
  }

  /**
   * {@inheritdoc}
   *
   * So we can create our special field mapping class.
   * Overrides XMLMigration function to be able to use the IndesignXMLFieldMapping class with the migration
   *
   * @param string|null $destination_field
   *   machine-name of destination field
   * @param string|null $source_field
   *   name of source field
   * @param bool $warn_on_override
   *   Set to FALSE to prevent warnings when there's an existing mapping
   *   for this destination field.
   * @param bool $strip_tags
   *   Set to TRUE to strip tags from content
   * @return IndesignXMLFieldMapping
   *   IndesignXMLFieldMapping
   */
  public function addFieldMapping($destination_field, $source_field = NULL,
                                  $warn_on_override = TRUE) {
    // Warn of duplicate mappings.
    if ($warn_on_override && !is_null($destination_field) && isset($this->codedFieldMappings[$destination_field])) {
      self::displayMessage(
        t('!name addFieldMapping: !dest was previously mapped, overridden',
          array('!name' => $this->machineName, '!dest' => $destination_field)),
        'warning');
    }
    $mapping = new IndesignXMLFieldMapping($destination_field, $source_field);
    if (is_null($destination_field)) {
      $this->codedFieldMappings[] = $mapping;
    }
    else {
      $this->codedFieldMappings[$destination_field] = $mapping;
    }
    return $mapping;
  }

  /**
   * {@inheritdoc}
   *
   * A normal $data_row has all the input data as top-level fields - in this
   * case, however, the data is embedded within a SimpleXMLElement object in
   * $data_row->xml. Explode that out to the normal form, and pass on to the
   * normal implementation.
   */
  protected function applyMappings() {
    // We only know what data to pull from the xpaths in the mappings.
    foreach ($this->getFieldMappings() as $mapping) {
      $source = $mapping->getSourceField();
      if ($source && !isset($this->sourceValues->{$source})) {
        $xpath = $mapping->getXpath();
        $strip_tags = $mapping->getStripTags();
        if ($xpath) {
          // Derived class may override applyXpath().
          $source_value = $this->applyXpath($this->sourceValues, $xpath, $strip_tags);
          if (!is_null($source_value)) {
            $this->sourceValues->$source = $source_value;
          }
        }
      }
    }
    parent::applyMappings();
  }

  /**
   * Gets item from XML using the xpath.
   * Overriding function in XMLMigration to be able to remove inline tags from the XML
   *
   * Default implementation - straightforward xpath application
   *
   * @param stdClass $data_row
   *   row containing items.
   * @param string $xpath
   *   xpath used to find the item
   * @param bool $strip_tags
   *   Set to TRUE to strip tags from content
   * @return \SimpleXMLElement found element
   * found element
   */
  public function applyXpath($data_row, $xpath, $strip_tags = FALSE) {
    if (!$strip_tags) {
      return parent::applyXpath($data_row, $xpath);
    }
    //strip_tags is TRUE, so remove all inline tags
    $result = $data_row->xml->xpath($xpath);
    if ($result) {
      if (count($result) > 1) {
        $return = array();
        foreach ($result as $record) {
          $record_string = $record->asXML();
          $record_string = strip_tags($record_string); //remove all tags from content
          $return[] = $record_string;
        }
        return $return;
      }
      else {
        $record_string = $result[0]->asXML(); //remove all tags from content
        $record_string = strip_tags($record_string); //remove all tags from content
        return $record_string;
      }
    }
    else {
      return NULL;
    }
  }
}

/**
 * Class IndesignXMLFieldMapping
 */
class IndesignXMLFieldMapping extends MigrateXMLFieldMapping {
  protected $strip_tags;

  /**
   * Sets stripTags to true, so that the content can be stripped of any inline XML tags
   * @return $this
   */
  public function stripTags() {
    $this->strip_tags = TRUE;
    return $this;
  }

  /**
   * Get stripTags value
   * @return mixed
   */
  public function getStripTags() {
    return $this->strip_tags;
  }

  /**
   * {@inheritdoc}
   * @param string $xpath - the xpath
   * @return \IndesignXmLFieldMapping
   */
  public function xpath($xpath) {
    return parent::xpath($xpath);
  }
}
