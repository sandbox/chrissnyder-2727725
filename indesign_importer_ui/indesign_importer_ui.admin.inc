<?php
/**
 * @file
 * Provides an admin form for the InDesign Importer UI module.
 * This form is used by content administrators to import an InDesign XML file into Drupal nodes.
 */

/**
 * Admin form for uploading and importing InDesign files
 * @param $form
 * @param $form_state
 */
function indesign_importer_ui_upload_form($form, &$form_state) {
  $form['issue_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of Issue/Volume'),
    '#description' => t('Name should be unique'),
    '#required' => TRUE,
  );
  $form['migration_registration'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine Name'),
    '#required' => TRUE,
    '#machine_name' => array(
      'exists' => 'indesign_importer_ui_migration_exists',
      'source' => array('issue_name'),
    ),
  );

  //We want to construct an issue/volume node form which can be used to gather node info
  //http://eosrei.net/articles/2011/08/get-renderable-array-single-field-instance
  $form['group_node'] = array(
    '#type' => 'fieldset',
    '#title' => t('Issue/Volume Information'),
  );

  $entity = 'node'; //todo - do we want to abstract this to apply to any entity?
  $bundle = variable_get('indesign_importer_ui_group_content_type');
  $form_state['group_bundle'] = $bundle;
  if (!empty($bundle)) { //if there is a group content set
    //we need to exclude any user excluded fields
    $group_fields = variable_get('indesign_importer_ui_group_fields');
    foreach ($group_fields as $group_field) {
      $fields = field_info_instances($entity, $bundle);
      foreach ($fields as $field_name => $field_value) {
        if (in_array($field_name, $group_fields)) {
          $form['#parents'] = array();
          $field = field_info_field($field_name);
          $ct_instances = field_info_instances($entity, $bundle);
          $instance = $ct_instances[$field_name];
          $items = $instance['default_value'];
          $addition = field_default_form(NULL, NULL, $field, $instance, LANGUAGE_NONE, $items, $form, $form_state);
          $form['group_node'] += $addition;
        }
      }
    }

  }
  else {
    global $base_root;
    drupal_set_message('Missing Group Content fields. Please configure at ' . l($base_root . '/admin/content/indesign/settings', $base_root . '/admin/content/indesign/settings'), 'error');
    $form['group_node']['missing_group'] = array(
      '#markup' => '<div class="missing-group">N/A</div>'
    );
  }

  $form['xml_upload'] = array(
    '#title' => t('InDesign XML'),
    '#type' => 'file',
    '#description' => t('Upload the XML file as exported from InDesign'),
  );
  $form['image_uploads'] = array(
    '#title' => t('InDesign Images'),
    '#type' => 'file',
    '#attributes' => array('multiple' => 'multiple'),
    '#name' => 'files[]',
    '#description' => t("Upload the images associated with the InDesign file - valid extensions: png gif jpg jpeg. Maximum file upload set at: " . ini_get('max_file_uploads')),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#title' => t('Submit'),
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Validate callback function for indesign_importer_ui_upload_form
 * @param $form
 * @param $form_state
 */
function indesign_importer_ui_upload_form_validate($form, &$form_state) {
  //https://coderwall.com/p/rsv9ra/d7-multiple-file-upload?p=1&q=author%3Atobiasdeardorff
  //todo - will need to break up the files into ones which contain multiple and ones which don't
  //Save multiple files
  $files = $_FILES['files']['name'];
  $max_file_uploads = intval(ini_get('max_file_uploads'));
  if (count($files) > $max_file_uploads) {
    form_error($form['image_uploads'], 'File upload exceeds the maximum limit of ' . $max_file_uploads);
    return;
  }
  $image_files = array();
  $xml_file_key = array();
  foreach ($files as $key => $value) {
    if (is_int($key)) {
      $image_files[] = $key; //this collects all the image files, which all have a numeric array key
    }
    else {
      $xml_file_key = $key;
    }
  }
  $machine_name = $form_state['values']['migration_registration'];
  //create file directories to hold uploaded files
  $new_public_path = 'public://indesign/' . $machine_name;
  $new_private_path = 'private://indesign/' . $machine_name;
  if (!file_prepare_directory($new_public_path, FILE_CREATE_DIRECTORY)) { //attempts to create file directory
    form_error($form, 'Public file directory is not writable');
  }
  if (!file_prepare_directory($new_private_path, FILE_CREATE_DIRECTORY)) { //attempts to create file directory
    form_error($form, 'Private file directory is not writable');
  }
  //first upload the xml file
  $xml_file = file_save_upload($xml_file_key, array(
    'file_validate_extensions' => array('xml'),
  ), $new_private_path);
  if ($xml_file) {
    $form_state['files']['xml'] = $xml_file;
    //Prepare the XML for the importer by adding a unique ID tag per article
    indesign_importer_ui_prepare_xml($xml_file);
    file_save($xml_file);
  }
  foreach ($image_files as $key => $value) {
    $file = file_save_upload($key, array(
      'file_validate_is_image' => array(),
      'file_validate_extensions' => array('png gif jpg jpeg'),
    ), $new_public_path);
    if ($file) {
      $form_state['files']['images'][] = $file;
      file_save($file);
    }
  }
}

/**
 * Submit callback function for indesign_importer_ui_upload_form
 * @param $form
 * @param $form_state
 */
function indesign_importer_ui_upload_form_submit($form, &$form_state) {
  //STEP 1
  //Retrieve the upload paths to provide the source for the importer migration.
  $xml_uri = $form_state['files']['xml']->uri;
  $xml_file = drupal_realpath($xml_uri);
  $machine_name = $form_state['values']['migration_registration'];
  $class_name = 'DefaultIndesignXMLMigration'; //This variable represents a real class name. The default value should not be used and should be overridden with the provided hook
  drupal_alter('indesign_importer_ui_migrate_class', $class_name);
  $group_name = 'indesign';
  //STEP 2
  //Save the issue/volume node and retrieve the nid
  $nid = indesign_importer_ui_get_issue($form_state);
  //STEP 3
  //Register the migration
  if (!empty($xml_file)) {
    //by registering a migration, it first deletes any existing mapping from past
    //imports and starts fresh with the new one
    Migration::registerMigration($class_name, $machine_name,
      array(
        'source_file' => $xml_file,
        'group_name' => $group_name,
        'issue_nid' => $nid,
        'images' => $form_state['files']['images']
      ));
  }

  //STEP 4
  //redirect them to the migrate page, or a new migrate page, if deemed fit
  drupal_goto('admin/content/migrate/groups/indesign');

}

/**
 * Determines if a Migrate migration already exists for the given machine name
 * @param $value - the machine name
 * @return bool - exists or not
 */
function indesign_importer_ui_migration_exists($value) {
  $exists = FALSE;
  $result = db_select('migrate_status', 'mfm')
    ->fields('mfm', array('machine_name', 'class_name', 'group_name'))
    ->condition('machine_name', $value, '=')
    ->execute();
  while ($row = $result->fetchAssoc()) {
    //check to see if the name is already in use
    $exists = TRUE;
  }
  if ($exists) {
    drupal_set_message('Machine name already exists, chose another');
  }
  return $exists;
}

/**
 * Creates a issue/volume node and returns the nid
 * @param array $form_state - the full $form_state array
 * @return mixed - the node nid
 */
function indesign_importer_ui_get_issue($form_state) {
  $group_fields = array();
  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value)) {
      $group_fields[$key] = $value;
    }
  }
  $group_bundle = $form_state['group_bundle'];
  $title = $form_state['values']['issue_name'];
  //create the node
  $node = entity_create('node', array('type' => $group_bundle));
  $node->uid = 1;
  $node->status = 0;
  $node->title = $title;
  foreach ($group_fields as $field_name => $field) {
    $node->{$field_name} = $field;
  }
  $node_wrapper = entity_metadata_wrapper('node', $node);
  $node_wrapper->save();
  $nid = $node_wrapper->getIdentifier();
  drupal_set_message('Issue successfully saved.');
  return $nid;
}

/**
 * Prepares the XML for the importer by adding a unique ID tag per article
 * @param object $xml_file - a Drupal file object
 */
function indesign_importer_ui_prepare_xml($xml_file) {
  //load the XML
  $path = drupal_realpath($xml_file->uri);
  $xml = new DOMDocument();
  $xml->load($path);
  //loop through the XML and add a unique ID tag per article
  $articles = $xml->getElementsByTagName('ArticleContainer');
  $count = 0;
  foreach ($articles as $article) {
    $article_id = $xml->createElement('ArticleId');
    $article_id_value = $xml->createTextNode($count);
    $article_id->appendChild($article_id_value);
    $article->appendChild($article_id);
    $count++;
  }
  $updated_xml = $xml->saveXML();

  file_save_data($updated_xml, $xml_file->uri, FILE_EXISTS_REPLACE); //update the file with the new tags
}