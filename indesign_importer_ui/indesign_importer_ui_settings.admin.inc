<?php
/**
 * @file
 * Provides an admin settings form for the InDesign Importer UI module
 */

/**
 * Form constructor for configuring the InDesign XML Importer.
 */
function indesign_importer_ui_settings_form($form, &$form_state) {
  $form['indesign_importer_ui_group_content_type'] = array(
    '#type' => 'select',
    '#title' => t('Group content type'),
    '#options'  => _indesign_importer_ui_get_content_types(),
    '#description' => t('ie: Issue/Volume content type'),
    '#default_value' => variable_get('indesign_importer_ui_group_content_type'),
    '#required' => TRUE,
    '#ajax' => array(
      'event' => 'change',
      'callback' => '_indesign_importer_ui_ajax_group_fields',
      'wrapper' => 'group-fields-wrapper',
    ),
  );

  // This allows us to update the list of group fields with ajax when the
  // group node is changed. This is the wrapper div that actually gets updated.
  $form['wrapper'] = array(
    '#prefix' => '<div id="group-fields-wrapper">',
    '#suffix' => '</div>',
  );

  // Here we want to check if the form state has a value, and update the fields
  // based on that. If not, then we check to see if there is already a saved
  // value and use the fields from that.
  $options = array();
  if (isset($form_state['values']['indesign_importer_ui_group_content_type'])) {
    $options = _indesign_importer_ui_get_content_type_fields($form_state['values']['indesign_importer_ui_group_content_type']);
  }
  elseif (!is_null(variable_get('indesign_importer_ui_group_content_type'))) {
    $options = _indesign_importer_ui_get_content_type_fields(variable_get('indesign_importer_ui_group_content_type'));
  }

  // This include the actual form item on the page; this concludes the ajax
  // stuff.
  $form['wrapper']['indesign_importer_ui_group_fields'] = array(
    '#type' => 'select',
    '#title' => t('Group fields'),
    '#description' => t('Choose the fields which will show up on the InDesign Importer UI page. You want to exclude any reference fields to items which are to be imported.'),
    '#options' => $options,
    '#default_value' => variable_get('indesign_importer_ui_group_fields'),
    '#multiple' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Return an array of available content types.
 *
 * The array is keyed by machine name and the values are the human readable
 * names.
 */
function _indesign_importer_ui_get_content_types() {
  $content_types = array();
  foreach (node_type_get_types() as $key => $content_type) {
    $content_types[$key] = $content_type->name;
  }
  return $content_types;
}

/**
 * Return the associated fields of a given content type.
 */
function _indesign_importer_ui_get_content_type_fields($content_type) {
  if (is_null($content_type)) {
    return array();
  }
  else {
    $fields = array();
    foreach (field_info_instances('node', $content_type) as $key => $field) {
      $fields[$key] = $field['label'];
    }
    return $fields;
  }
}

/**
 * Ajax callback for the group node fields configuration.
 */
function _indesign_importer_ui_ajax_group_fields($form, &$form_state) {
  return $form['wrapper'];
}