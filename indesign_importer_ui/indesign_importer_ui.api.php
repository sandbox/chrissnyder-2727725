<?php
/**
 * @file
 * Provides hooks for the InDesign Importer UI module
 */

/**
 * Alters and overrides the default Migrate class for teh InDesign Importer UI
 * @param $class_name
 */
function hook_indesign_importer_ui_migrate_class_alter(&$class_name) {
}